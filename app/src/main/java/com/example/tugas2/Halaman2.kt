package com.example.tugas2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class Halaman2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_halaman2)

        val intent = intent
        if (intent != null) {
            val bundle = intent.getBundleExtra("bundle")
            val getNama = bundle?.getString("nama")
            val getJk = bundle?.getString("jenisKelamin")
            val getProdi = bundle?.getString("prodi")

            val outNama = findViewById<TextView>(R.id.outNama)
            outNama.text = getNama
            val outJk = findViewById<TextView>(R.id.outJk)
            outJk.text = getJk
            val outProdi = findViewById<TextView>(R.id.outProdi)
            outProdi.text = getProdi
        }

        val back = findViewById<Button>(R.id.bttn2)
        back.setOnClickListener {
            finish()
        }
    }
}