package com.example.tugas2

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    var ProgramStudi = arrayOf<String>("Teknik Informatika", "Teknik Komputer", "Sistem Informasi", "Teknologi Informasi", "Pendidikan Teknologi Informasi")
        @SuppressLint("WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val pilihProdi = findViewById<Spinner>(R.id.prodi)
        pilihProdi.onItemSelectedListener = this

        val ad: ArrayAdapter<String> = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            ProgramStudi)

        ad.setDropDownViewResource(
            android.R.layout.simple_spinner_dropdown_item)

        pilihProdi.adapter = ad

        val masuk = findViewById<Button>(R.id.bttn)
        masuk.setOnClickListener {
            nextHal()
        }
    }

    private fun nextHal() {
        val intent = Intent(this, Halaman2::class.java)

        val nama = findViewById<TextView>(R.id.nama).text.toString()
        val btngroup = findViewById<RadioGroup>(R.id.radioGroup).checkedRadioButtonId
        val radioButton : RadioButton = findViewById(btngroup)
        val jenisKelamin = radioButton.text.toString()
        val programStudi = findViewById<Spinner>(R.id.prodi).selectedItem.toString()
        val bundle = Bundle().apply {
            putString("nama", nama)
            putString("jenisKelamin", jenisKelamin)
            putString("prodi", programStudi)
        }

        intent.putExtra("bundle", bundle)
        startActivity(intent)
    }

    override fun onRestart() {
        super.onRestart()
        findViewById<TextView>(R.id.nama).text = ""
        findViewById<RadioGroup>(R.id.radioGroup).clearCheck()
        findViewById<Spinner>(R.id.prodi).setSelection(0)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}
}